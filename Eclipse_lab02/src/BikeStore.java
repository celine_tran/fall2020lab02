// Celine Tran 1938648
public class BikeStore {

	public static void main(String[] args) {
		
		Bicycle[] bicycles = new Bicycle[4];
		
	    bicycles[0] = new Bicycle("Specialized",21,40);
	    bicycles[1] = new Bicycle("BikeCo",19,45);
	    bicycles[2] = new Bicycle("PedalLite",15,50);
	    bicycles[3] = new Bicycle("CycleBi",30,41);
	       
	    printArr(bicycles);      
	}
	
	public static void printArr(Bicycle[]bicycles){
        for(int i=0;i<bicycles.length;i++){
          System.out.println(bicycles[i].toString()); 
        }
      }
}
