// Celine Tran 1938648
public class Bicycle {
private String manufacturer;
private int numberGears;
private double maxSpeed;

public String getManufacturer() {
	return manufacturer;
}

public int getNumberGears() {
	return numberGears;
}

public double getMaxSpeed() {
	return maxSpeed;
}


public Bicycle (String manufacturer, int numberGears,double maxSpeed) {
this.manufacturer = manufacturer;
this.numberGears = numberGears;
this.maxSpeed = maxSpeed;
}

public String toString(){
	return ("Manufacturer:" + getManufacturer() + ", number of gears: "+ getNumberGears() + ", Maximum speed:" + getMaxSpeed());
}
}